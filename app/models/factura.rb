class Factura < ActiveRecord::Base
	belongs_to :usuario, :foreign_key=>"id_usuario"
	has_many :Detalle_factura, :foreign_key=>"id_factura"

	#validates_presence_of :codigo
	validates_presence_of :id_usuario
	validates_presence_of :cliente
	validates_presence_of :fecha
	validates_presence_of :total

	#validates_uniqueness_of :codigo
end
