class DetalleFactura < ActiveRecord::Base
	belongs_to :Factura, :foreign_key=>"id_factura"
	belongs_to :Factura, :foreign_key=>"id_articulo"

	validates_presence_of :id_factura
	validates_presence_of :id_articulo
	validates_presence_of :cantidad
	validates_presence_of :subtotal
end
