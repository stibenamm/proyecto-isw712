class Usuario < ActiveRecord::Base
	has_many :Factura, :foreign_key=>"id_usuario"
	has_many :Articulo, :foreign_key=>"id_usuario"
	validates_presence_of :nombre
	validates_presence_of :contrasena
end
