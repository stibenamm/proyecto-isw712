class Articulo < ActiveRecord::Base
	belongs_to :usuario, :foreign_key=>"id_usuario"
	has_many :Detalle_factura, :foreign_key=>"id_articulo"

	validates_presence_of :codigo
	validates_presence_of :id_usuario
	validates_presence_of :descripcion
	validates_presence_of :precio_compra
	validates_presence_of :precio_venta
	validates_presence_of :stock
	validates_uniqueness_of :codigo
end
