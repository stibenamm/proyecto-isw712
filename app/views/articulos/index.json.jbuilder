json.array!(@articulos) do |articulo|
  json.extract! articulo, :id, :id_usuario, :codigo, :descripcion, :precio_compra, :precio_venta, :stock
  json.url articulo_url(articulo, format: :json)
end
