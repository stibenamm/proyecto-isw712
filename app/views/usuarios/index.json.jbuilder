json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :nombre, :contrasena
  json.url usuario_url(usuario, format: :json)
end
