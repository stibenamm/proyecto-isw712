json.array!(@detalle_facturas) do |detalle_factura|
  json.extract! detalle_factura, :id, :id_factura, :id_articulo, :cantidad, :subtotal
  json.url detalle_factura_url(detalle_factura, format: :json)
end
