json.array!(@facturas) do |factura|
  json.extract! factura, :id, :id_usuario, :cliente, :fecha, :total
  json.url factura_url(factura, format: :json)
end
