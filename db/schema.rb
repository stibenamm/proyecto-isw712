# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140407052534) do

  create_table "articulos", force: true do |t|
    t.integer  "id_usuario"
    t.string   "codigo"
    t.string   "descripcion"
    t.decimal  "precio_compra"
    t.decimal  "precio_venta"
    t.integer  "stock"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "detalle_facturas", force: true do |t|
    t.integer  "id_factura"
    t.integer  "id_articulo"
    t.integer  "cantidad"
    t.decimal  "subtotal"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "facturas", force: true do |t|
    t.integer  "id_usuario"
    t.string   "cliente"
    t.date     "fecha"
    t.decimal  "total"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nombre"
    t.string   "contrasena"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
