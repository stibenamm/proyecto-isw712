class CreateDetalleFacturas < ActiveRecord::Migration
  def change
    create_table :detalle_facturas do |t|
      t.integer :id_factura
      t.integer :id_articulo
      t.integer :cantidad
      t.decimal :subtotal

      t.timestamps
    end
  end
end
