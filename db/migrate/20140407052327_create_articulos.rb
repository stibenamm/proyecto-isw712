class CreateArticulos < ActiveRecord::Migration
  def change
    create_table :articulos do |t|
      t.integer :id_usuario
      t.string :codigo
      t.string :descripcion
      t.decimal :precio_compra
      t.decimal :precio_venta
      t.integer :stock

      t.timestamps
    end
  end
end
