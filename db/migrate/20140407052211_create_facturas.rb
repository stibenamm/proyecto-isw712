class CreateFacturas < ActiveRecord::Migration
  def change
    create_table :facturas do |t|
      t.integer :id_usuario
      t.string :cliente
      t.date :fecha
      t.decimal :total

      t.timestamps
    end
  end
end
